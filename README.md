### Downloading backups via FTP

Reads yaml file - config/<config_name>\
Set name:
~~~~~~~~
    BACKUP_FTP_CONFIG=config.yaml
~~~~~~~~
Sample file content:
~~~~~~~~
server1:
    address: <ip>
    username: <username>
    password: <password>
    source: <path to directory in ftp>
    destination: <path to local directory>
    days: <>
server2:
    address: <ip>
    username: <username>
    password: <password>
    source: <path to directory in ftp>
    destination: <path to local directory>
    days: <>
~~~~~~~~

Alternale configuration v1, set variables:
~~~~~~~~
    BACKUP_FTP_ADDRESS
    BACKUP_FTP_USERNAME
    BACKUP_FTP_PASSWORD
    BACKUP_FTP_SOURCE
    BACKUP_FTP_DEST
    BACKUP_FTP_DAYS
~~~~~~~~

Send to server
