import yaml, os, sys
from ftplib import FTP
import platform, time
from os import walk

class download_from_ftp():

    def __init__(self):
        if 'BACKUP_FTP_CONFIG' in os.environ:
            file = os.path.join(os.path.dirname(__file__), 'config', os.environ['BACKUP_FTP_CONFIG'])
            with open(file) as f:
                self.params = yaml.safe_load(f)
        else:
            try:
                self.params = {'server1':{'address': os.environ['BACKUP_FTP_ADDRESS'],
                                          'username': os.environ['BACKUP_FTP_USERNAME'],
                                          'password': os.environ['BACKUP_FTP_PASSWORD'],
                                          'source': os.environ['BACKUP_FTP_SOURCE'],
                                          'destination': os.environ['BACKUP_FTP_DEST'],
                                          'days': os.environ['BACKUP_FTP_DAYS']}}
            except KeyError:
                print ('Variables is not defined',file=sys.stderr)
                exit(2)
    def __connect_to_ftp__(self):
            self.ftp = FTP(self.addr)
            self.ftp.login(self.user, self.password)
            self.ftp.cwd(self.source)
    def __listFilesOnDst__(self):
        f = []
        for (dirpath, dirnames, filenames) in walk(self.destination):
            f.extend(filenames)
            break
        return f

    def __getArchiveList__(self):
        a = set(self.ftp.nlst()[2:-1])
        b = set(self.__listFilesOnDst__())
        c = (a-b)
        listSub = list(c) if c else ()
        return listSub

    def __getArchives__(self):
        archive_list = self.__getArchiveList__()
        for archive in archive_list:
            local_filename = os.path.join(self.destination, archive)
            with open(local_filename, "wb") as lf:
                self.ftp.retrbinary("RETR " + archive, lf.write, 8 * 1024)
        return archive_list

    def download_archive(self):
        archive_list = []
        for server in self.params:
            self.addr = self.params[server]['address']
            self.user= self.params[server]['username']
            self.password = self.params[server]['password']
            self.source = self.params[server]['source']
            self.destination = self.params[server]['destination']
            self.days = int(self.params[server]['days'])
            self.__connect_to_ftp__()
            archive_list+= self.__getArchives__()
            self.ftp.close()
            del self.ftp
            self.removeOldArchive(self.destination, self.days)
        return archive_list

    def secondInDay(self,days):
        return 60 * 60 * 24 * days

    def creation_date(self, path_to_file):
        """
        Try to get the date that a file was created, falling back to when it was
        last modified if that isn't possible.
        See http://stackoverflow.com/a/39501288/1709587 for explanation.
        """
        if platform.system() == 'Windows':
            return os.path.getctime(path_to_file)
        else:
            stat = os.stat(path_to_file)
            try:
                return stat.st_birthtime
            except AttributeError:
                # We're probably on Linux. No easy way to get creation dates here,
                # so we'll settle for when its content was last modified.
                return stat.st_mtime

    def removeOldArchive(self, destination, days):
        for file in self.__listFilesOnDst__():
             local_filename = os.path.join(destination, file)
             timestamp_to_deleted = self.secondInDay(days) + self.creation_date(local_filename)
             if timestamp_to_deleted <= time.time():
                 os.remove(local_filename)

if __name__ == "__main__":
    print("Script is running...",file=sys.stdout)
    archives = download_from_ftp()
    while True:
        list_archive = archives.download_archive()
        if not list_archive:
            print("INFO: List archive is empty!")
        if list_archive:
            print("INFO: Archives to download:")
            for list in list_archive:
                print(list)
        update_interval=1800
        try:
            update_interval=int(os.environ['BACKUP_FTP_UPDATE_INTERVAL'])
        except KeyError:
            print ('WARN: BACKUP_FTP_UPDATE_INTERVAL is not defined')
        time.sleep(update_interval)








